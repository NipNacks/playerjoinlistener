package me.nipnacks.playerjoinlistener;

import org.bukkit.plugin.java.JavaPlugin;

public final class PlayerJoinListener extends JavaPlugin {

    @Override
    public void onEnable() {
        // Plugin startup logic
        System.out.println("PlayerJoinListener by NipNacks is now starting up");

        getConfig().options().copyDefaults();
        saveDefaultConfig();

        getServer().getPluginManager().registerEvents(new PlayerFirstJoinEvent(this), this);
    }

    @Override
    public void onDisable() {
        // Plugin shutdown logic
    }
}
