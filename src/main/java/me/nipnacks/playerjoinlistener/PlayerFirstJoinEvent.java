package me.nipnacks.playerjoinlistener;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class PlayerFirstJoinEvent implements Listener {

    PlayerJoinListener plugin;

    public PlayerFirstJoinEvent(PlayerJoinListener plugin){
        this.plugin = plugin;
    }

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent event){
        Player player = event.getPlayer();

        String playerfirstjoin = plugin.getConfig().getString("playerfirstjoin");
        String playerfirstjoinwebsite = plugin.getConfig().getString("playerfirstjoinwebsite");
        String playerfirstjoinstore = plugin.getConfig().getString("playerfirstjoinstore");
        String playerfirstjoindiscord = plugin.getConfig().getString("playerfirstjoindiscord");
        String discordlink = plugin.getConfig().getString("discordlink");
        String storelink = plugin.getConfig().getString("storelink");
        String websitelink = plugin.getConfig().getString("websitelink");

        if(player.hasPlayedBefore() == false){
            player.sendMessage(ChatColor.translateAlternateColorCodes('&', playerfirstjoin + event.getPlayer().getDisplayName() ));
            player.sendMessage("");
            player.sendMessage(ChatColor.translateAlternateColorCodes('&', playerfirstjoinstore + storelink));
            player.sendMessage(ChatColor.translateAlternateColorCodes('&', playerfirstjoindiscord + discordlink));
            player.sendMessage(ChatColor.translateAlternateColorCodes('&', playerfirstjoinwebsite + websitelink));
            Bukkit.broadcastMessage(ChatColor.GOLD + "[" + ChatColor.YELLOW + "+" + ChatColor.GOLD + "]" + event.getPlayer().getDisplayName());
        } else {
            Bukkit.broadcastMessage(ChatColor.GOLD + "[" + ChatColor.YELLOW + "+" + ChatColor.GOLD + "]" + event.getPlayer().getDisplayName());
        }
    }

    @EventHandler
    public void onPlayerLeave(PlayerQuitEvent event){
        Player player = event.getPlayer();

        Bukkit.broadcastMessage(ChatColor.GOLD + "[" + ChatColor.YELLOW + "-" + ChatColor.GOLD + "]" + event.getPlayer().getDisplayName());
    }
}
